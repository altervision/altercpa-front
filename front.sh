#!/bin/bash

# Usage for AlterCPA:
# wget https://cpa.st/setup/front.sh
# bash front.sh <r.hostname.com> <hostname.com> <siteapikey>

# Usage for generic proxy:
# wget https://cpa.st/setup/front.sh
# bash front.sh <r.hostname.com> "<http://url.to/domains/list.txt>"

# Get the hostname from parameters
if [ -z "$1" ]; then
	echo "No AlterCPA parking domain specified"
	exit 1
else
	ACPAURL="$1"
fi

# Get the API key from parameters
if [ -z "$2" ]; then
	echo "No AlterCPA domain or domains list URL specified"
	exit 1
else
	ACPAKEY="$2"
fi

# Get the API key from parameters
if [ -z "$3" ]; then
	echo "Using full URL to domains: ${ACPAKEY}"
else
	ACPAKEY="https://${ACPAKEY}/api/site/domain.raw?token=${3}"
	echo "Using AlterCPA domain list: ${ACPAKEY}"
fi

# Add the IP to hosts
if [ -z "$4" ]; then
	echo "${4} ${2} ${1}" >> /etc/hosts
fi

# Update system and install all the components
apt-get -y update
apt-get -y upgrade
apt-get -y install ca-certificates apt-transport-https
apt-get -y install coreutils mc nano net-tools curl nginx zip unzip

# Make necessary directories
cd /root
mkdir -p /var/www
mkdir -p /var/www/acme
mkdir -p /var/www/cache
mkdir -p /var/www/data
mkdir -p /root/cert

# Create WWW user for FTP access
groupadd -g 1001 wsvr
useradd -g 1001 -u 1001 -d /var/www wsvr

# Load configuration archive
ufw disable
wget -q https://cpa.st/setup/debian-front.zip
unzip -o -qq debian-front.zip -d /

# Change change passwords in configuration files
sed -i "s/r.domain.com/$ACPAURL/g" /etc/nginx/snippets/proxy.conf
echo "#!/bin/bash" > /root/config
echo "THEDOMAINURL=\"${ACPAKEY}\"" >> /root/config

# Change file permissions
chmod a+x /root/acme/dehydrated
chmod a+x /root/config
chmod a+x /root/webssl
chmod a+x /root/uncache
chown -R wsvr:wsvr /var/www

# Restart all the services
service nginx restart

# Download content
/root/acme/dehydrated --config /root/acme/config --register --accept-terms
nohup openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048 &

# Root cron processor
echo "*/30 * * * * bash /root/webssl >/dev/null 2>&1" > /var/spool/cron/crontabs/root
crontab /var/spool/cron/crontabs/root
