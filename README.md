# AlterVision CPA Front Proxy

**AlterCPA Front** is domain parking front-server deployment script for AlterCPA Pro

**AlterCPA Фронт** - это скрипт настройки фронт-сервера для парковки доменов в AlterCPA Pro.

## Setting up

You'll need AlterCPA system parking domaom, home domain and generic site API-key to setup the server. Use them instead of `r.domain.com`, `domain.com` and `siteapikey`.

Для установки вам потребуется системный парковочный домен, адрес домена, где установлена AlterCPA, и API-ключ парковки. Укажите их вместо `r.domain.com`, `domain.com` и `siteapikey`.

```bash
wget https://cpa.st/setup/front.sh
bash front.sh r.domain.com domain.com siteapikey
```

## Developer

- WWW: https://www.altercpa.pro
- Email: me@wr.su
- Telegram: [@altercpapro](https://t.me/altercpapro)
- Skype: altervision13